import React from 'react';
import { Link } from 'react-router-dom';


const Navbar = () => {
    return (
        <div className="container">
        <nav className="navbar navbar-dark teal navbar-expand-lg">
            <div className="collpase navbar-collapse">
                <ul className="navbar-nav mr-auto">
                    <li className="navbar-item">
                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    <li className="navbar-item right">
                        <Link to="/contact" className="nav-link">Contact</Link>
                    </li>
                </ul>
            </div>
        </nav>
        </div>
    );
};

export default Navbar;
