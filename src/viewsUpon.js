'use strict';

import React, { Component } from 'react';
import Axios from 'axios';
import TagList from './TagList';

/**
*views upon component
**/
class viewsUpon extends Component {
    /**
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.state = {
            dumpTagsAndTweets: [],
        };
    }
    /**
     * @set {state}
     */
    async componentDidMount() {
        try {
            const tagResponse = await Axios.get('https://www.tronalddump.io/tag');
            // console.log(tagResponse.data._embedded.tag);
            if (tagResponse.data._embedded.tag.length > 0) {
                /**
                 * set state with data response to dumpTag array
                 */
                this.setState({
                    showTags: true,
                    dumpTagsAndTweets: tagResponse.data._embedded.tag.map((eachDumpTag) => {
                        return (
                            { dumpTag: eachDumpTag.value, tweets: [] }
                        );
                    }),
                });
            }
        } catch (err) {
            console.log('error');
        }
    }
    /**
     *create all tweets component
     * @param {string} tagName
     */
    onClickOfTag = async (tagName) => {
        let isNeedToFetchTweets = true;
/**
 * logic for fetching tweets if necessary on click to tags
 */
        const dumpTagsAndTweets = this.state.dumpTagsAndTweets;
        dumpTagsAndTweets.forEach((eachTagAndTweets) => {
            if (eachTagAndTweets.tweets.length !== 0 && eachTagAndTweets.dumpTag === tagName) {
                isNeedToFetchTweets = false;
            }

            if (eachTagAndTweets.tweets.length !== 0) {
                eachTagAndTweets.tweets = [];
            }
        });
        this.setState({
            dumpTagsAndTweets,
        });
/**
 * logic for setting state with tweets quotes fetched from API
 */
        if (isNeedToFetchTweets) {
            try {
                const tweetsResponse = await Axios.get(`https://www.tronalddump.io/search/quote?tag=${tagName}`);
                if (tweetsResponse.data._embedded.quotes.length > 0) {
                    const tweets = tweetsResponse.data._embedded.quotes.map((eachQuoteDetails) => {
                        // console.log(eachQuoteDetails.value);
                        return eachQuoteDetails.value;
                    });
                    // console.log(tweets);

                    dumpTagsAndTweets.forEach((eachTagAndTweets) => {
                        if (eachTagAndTweets.dumpTag === tagName) {
                            eachTagAndTweets.tweets = tweets;
                        }
                    });
                    this.setState({
                        dumpTagsAndTweets: dumpTagsAndTweets,
                    });
                    // console.log(this.state);
                }
            } catch (err) {
                console.log('error');
            }
        }
    }

    /**
     * @return {state}
     */
    render() {
        const tagListJSX = this.state.dumpTagsAndTweets.map((eachTagAndTweets) => {
            /**
             * sending onClickOfTag function, eachTagAndTweets and key via props
             */
            return (
                <TagList key={eachTagAndTweets.dumpTag} onClickOfTag={this.onClickOfTag} eachTagAndTweets={eachTagAndTweets} />
            );
        });

        const homeContent = this.state.dumpTagsAndTweets.length === 0 ? (
            <div className="progress">
                <div className="indeterminate"></div>
            </div>
        ) : (
            /**
             * returning tagListJSX constituent on homeContent component for conditions of length of dumpTagAndTweets > 0
             */
                <div className='collection center container'>
                    {tagListJSX}
                </div>
            );

        return (
            homeContent
        );
    }
}

export default viewsUpon;
