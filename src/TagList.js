/* eslint-disable max-len */
import React, { Component } from 'react';

const Tweet = ({ eachTweet }) => {
    return (
        <div className='collection-item'>
            {eachTweet}
        </div>
    );
};

/**
 * class TagList
 */
class TagList extends Component {
    /**
     * constructor
     */
    constructor() {
        super();
    }

    /**
     * @return {JSX}
     */
    render() {
        /**
         * logic for placing active className for tag with eachTagAndTweets.tweets length > 0
         */
        const shouldBeActive = this.props.eachTagAndTweets.tweets.length > 0;
        const classNameForTag = shouldBeActive ? 'collection-item active' : 'collection-item';
/**
 * list of tweets returned by mapping the props received
 */
        const tweetsJSX = this.props.eachTagAndTweets.tweets.length !== 0 ? (
            this.props.eachTagAndTweets.tweets.map((eachTweet) => {
                return (<Tweet key={Math.random()} eachTweet={eachTweet} />);
            })
        ) : (
                <div></div>
            );

        return (
            /**
             * returning jsx for tweets corresponding to their tags
             */
            <div>
                <a href="#!" className={classNameForTag} onClick={async () => {
                    this.props.onClickOfTag(this.props.eachTagAndTweets.dumpTag);
                }}>
                    {this.props.eachTagAndTweets.dumpTag}
                </a>

                {tweetsJSX}
            </div>
        );
    }
};

export default TagList;
