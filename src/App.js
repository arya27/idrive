import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Navbar from './Navbar';
import Contact from './contact';
import viewsUpon from './viewsUpon';

/**
 * Root component
 */
class App extends Component {
  /**
   * @return {JSX}
   */
  render() {
    return (
      <Router>
        <div>
        <Navbar />
          <Route path ='/' exact component={viewsUpon}/>
          <Route path='/contact' component={Contact}></Route>
        </div>
      </Router>
    );
  }
}

export default App;
